from django.db import models
from django.utils.translation import gettext_lazy as _


class Instrument(models.Model):
    """
    Instruments played (or not) by the band.
    Instruments has various properties, key, transposition.
    """

    class Meta:
        verbose_name = _('instrument')
        verbose_name_plural = _('instruments')

    # Instrument name. "Trombone", "Alto Saxophone", ...
    name = models.CharField(_('instrument name'), max_length=50)

    # Pitch of the instrument. Clarinets are transposing to Bb, for example.
    # We use american notation for the code. That may be translated later :
    # C - Do
    # Bb - Si bemol, ...
    pitch = models.CharField(_('pitch'), max_length=2)

    # Key of instrument.
    # For some brass instruments, the music sheet may be displayed
    #   in G key, or
    #   in F key
    key = models.CharField(_('key'), max_length=8)

    # An instrument belongs to a stand.
    # A bass tuba and a sousaphone are both playing the bass sheet.
    # They are both included in the bass stand.
    stand = models.ForeignKey("Stand", on_delete=models.PROTECT)


class Stand(models.Model):
    """
    Stands are sets of instruments.
    We did not choose yet if permissions are managed via the 'Group' class
    (permissions should be defined here)
    """

    class Meta:
        verbose_name = _('stand')
        verbose_name_plural = _('stands')

    # Stand name. If trumpets, bugles and cornets always play the same sheets,
    # then they are roughly "trumpets".
    name = models.CharField(_('stand name'), max_length=50)
