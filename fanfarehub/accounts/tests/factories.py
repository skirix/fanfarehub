import factory
from factory.django import DjangoModelFactory
from factory.faker import Faker


class UserFactory(DjangoModelFactory):
    first_name = Faker('first_name')
    last_name = Faker('last_name')
    email = factory.Sequence(lambda x: 'user{0}@example.org'.format(x))

    class Meta:
        model = 'accounts.User'

    @classmethod
    def _create(cls, model_class, *args, **kwargs):
        # override the default method to use a custom method from the manager
        # which is required to set the user's password properly
        return cls._get_manager(model_class).create_user(*args, **kwargs)
