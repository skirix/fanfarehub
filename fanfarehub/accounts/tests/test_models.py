import pytest

from ..models import User
from .factories import UserFactory


@pytest.mark.django_db
class TestUserManager:
    def test_create_user(self):
        personne = User.objects.create_user('mail@example.org')
        assert personne.is_superuser is False
        assert User.objects.get() == personne

    def test_create_superuser(self):
        admin = User.objects.create_superuser('mail@example.org', 'pass')
        assert admin.is_staff is True
        assert admin.is_superuser is True
        assert User.objects.get() == admin


class TestUser:
    def test_name_methods(self):
        personne = User(email="address@example.org")
        assert str(personne) == "address@example.org"

        personne.first_name = "Camille"
        personne.last_name = "Dupond"
        assert personne.get_short_name() == "Camille"
        assert personne.get_full_name() == "Camille Dupond"
        assert str(personne) == "Camille Dupond"

    def test_normalize_email(self):
        personne = UserFactory.build(email="Address@Example.Org")
        personne.clean()
        # cf. django.contrib.auth.base_user.BaseUserManager.normalize_email
        assert personne.email == "Address@example.org"

    def test_email_user(self, mailoutbox):
        personne = UserFactory.build()
        personne.email_user('subject', 'body')
        assert len(mailoutbox) == 1

    def test_is_staff(self):
        personne = UserFactory.build(is_superuser=True)
        assert personne.is_staff is True
        personne.is_superuser = False
        assert personne.is_staff is False
