import re

from django.contrib.messages import INFO as INFO_LEVEL
from django.contrib.messages import SUCCESS as SUCCESS_LEVEL
from django.urls import reverse, reverse_lazy

import pytest
from pytest_django.asserts import assertRedirects

from .factories import UserFactory

LOGIN_URL = reverse_lazy('accounts:login')
RE_URL = re.compile(r'http(|s)://[^/]*/([^ ]*/)$', re.MULTILINE)


@pytest.fixture
def test_user():
    return UserFactory(email='user@example.org', password='SkaYiddish')


@pytest.mark.django_db
class TestLoginLogout:
    def test_get(self, django_app):
        response = django_app.get(LOGIN_URL)
        assert response.status_code == 200

    def test_login(self, django_app, test_user):
        auth_form = django_app.get(LOGIN_URL).form
        auth_form['username'] = 'user@example.org'
        auth_form['password'] = 'SkaYiddish'
        response = auth_form.submit()
        assertRedirects(response, reverse('home'))

    def test_login_inactive(self, django_app, test_user):
        test_user.is_active = False
        test_user.save()

        auth_form = django_app.get(LOGIN_URL).form
        auth_form['username'] = 'user@example.org'
        auth_form['password'] = 'SkaYiddish'
        response = auth_form.submit()
        assert response.status_code == 200
        assert response.context['form'].errors

    def test_logout(self, django_app, test_user):
        response = django_app.get(reverse('accounts:logout'), user=test_user)
        assertRedirects(response, LOGIN_URL)
        # check that a success message has been added
        messages = list(response.follow().context['messages'])
        assert len(messages) == 1
        assert messages[0].level == SUCCESS_LEVEL


@pytest.mark.django_db
class TestPasswordReset:
    password_reset_url = reverse_lazy('accounts:password_reset')

    def test_workflow(self, django_app, mailoutbox):
        user = UserFactory(email='iforgot@example.org', password='')

        # 1. We request a password reset link for the user
        reset_form = django_app.get(self.password_reset_url).form
        reset_form['email'] = user.email
        response = reset_form.submit()
        # ... we are redirected to the sign in page
        assertRedirects(response, LOGIN_URL)
        # ... with an information message
        messages = list(response.follow().context['messages'])
        assert len(messages) == 1
        assert messages[0].level == INFO_LEVEL
        # ... an email has been sent
        assert len(mailoutbox) == 1
        # ... with the password reset link.
        confirm_url = RE_URL.search(mailoutbox[0].body).group()
        assert confirm_url

        # 2. We follow the link and confirm the reset
        reset_form = django_app.get(confirm_url).follow().form
        reset_form['new_password1'] = 'BellaCiao'
        reset_form['new_password2'] = 'BellaCiao'
        response = reset_form.submit()
        # ... we are redirected to the sign in page
        assertRedirects(response, LOGIN_URL)
        # ... we can now sign in with the new password.
        assert response.client.login(username=user.email, password='BellaCiao')

        # 3. The link is not valid anymore
        response = django_app.get(confirm_url)
        assert not response.forms

    def test_email_unknown(self, django_app, mailoutbox):
        # We request a password reset link for an unknown user
        reset_form = django_app.get(self.password_reset_url).form
        reset_form['email'] = 'idontexist@no.lan'
        response = reset_form.submit()
        # ... we are redirected to the sign in page
        assertRedirects(response, LOGIN_URL)
        # ... with an information message
        messages = list(response.follow().context['messages'])
        assert len(messages) == 1
        assert messages[0].level == INFO_LEVEL
        # ... but no email has been sent.
        assert len(mailoutbox) == 0

    def test_logged_in(self, django_app, test_user):
        response = django_app.get(self.password_reset_url, user=test_user)
        assertRedirects(response, reverse('accounts:password_change'))


@pytest.mark.django_db
class TestPasswordChange:
    url = reverse_lazy('accounts:password_change')

    def test_unauthorized(self, django_app):
        response = django_app.get(self.url)
        assert response.status_code == 302

    def test_change(self, django_app, test_user):
        change_form = django_app.get(self.url, user=test_user).form
        change_form['old_password'] = 'SkaYiddish'
        change_form['new_password1'] = 'BellaCiao'
        change_form['new_password2'] = 'BellaCiao'
        response = change_form.submit()
        # we are redirected and the password has changed
        assertRedirects(response, reverse('accounts:profile'))
        assert response.client.login(
            username=test_user.email, password='BellaCiao'
        )


@pytest.mark.django_db
class TestProfileEdit:
    url = reverse_lazy('accounts:profile')

    def test_unauthorized(self, django_app):
        response = django_app.get(self.url)
        assert response.status_code == 302

    def test_edit(self, django_app, test_user):
        response = django_app.get(self.url, user=test_user)
        assert set(response.context['form'].fields.keys()) == {
            'first_name',
            'last_name',
        }
        # make changes and submit the form
        edit_form = response.form
        edit_form['first_name'] = 'Toto'
        response = edit_form.submit()
        assertRedirects(response, self.url)
        # check that the first name has changed
        test_user.refresh_from_db()
        assert test_user.first_name == 'Toto'
