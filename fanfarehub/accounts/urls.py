from django.urls import path

from . import views

app_name = 'accounts'

urlpatterns = [
    path('login/', views.LoginView.as_view(), name='login'),
    path('logout/', views.LogoutView.as_view(), name='logout'),

    path('password/reset/',
         views.PasswordResetView.as_view(),
         name='password_reset'),
    path('password/reset/<uidb64>/<token>/',
         views.PasswordResetConfirmView.as_view(),
         name='password_reset_confirm'),
    path('password/',
         views.PasswordChangeView.as_view(),
         name='password_change'),

    path('profile/', views.ProfileEditView.as_view(), name='profile'),
]
